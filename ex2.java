package ex2;

import java.util.concurrent.TimeUnit;

class counter
{
    int count = 0;
    public synchronized int increment()
    {
        ++count;
        return count;
    }
}

public class ex2 {



    public static void main (String[] args)
    {
        counter c = new counter();
        Thread NewThread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                while(c.increment()<=11)
                {
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.print("NewThread1-"+ java.time.LocalTime.now() + "\n");

                }

            }
        });
        Thread NewThread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                while(c.increment()<=11)
                {
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.print("NewThread2-"+ java.time.LocalTime.now() + "\n");

                }

            }
        });
        Thread NewThread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                while(c.increment()<=11)
                {
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.print("NewThread3-"+ java.time.LocalTime.now() + "\n");

                }

            }
        });
        NewThread1.start();
        NewThread2.start();
        NewThread3.start();
    }
}
